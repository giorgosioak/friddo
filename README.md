# Friddo ( Frida Server Companion )

#### TODO:
- Get updates from frida github repository [https://api.github.com/repos/frida/frida/releases]
- Download version to device ( /tmp/local or app data folder and maybe custom path later on )
- Keep a page with the logs from runtime
- Clear downloads
- Run / Stop server from app
- Notification of runtime with options
- On boot run server option

#### Basic idea in draw:
![sic idea in draw](https://i.imgur.com/QqpVe3D.jpg)

#### Screenshots:
![Screenshot of app in alpha version](https://i.imgur.com/fRoLGGN.png)
